import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {PesananAktifIcon} from '../../assets';

const PesananAktif = ({noPesanan, statusPesanan}) => {
  const labelStatus = status => {
    if (status === 1) {
      return 'Sudah Selesai';
    } else {
      return 'Sedang Dicuci';
    }
  };

  return (
    <TouchableOpacity style={styles.container}>
      <PesananAktifIcon />
      <View style={styles.label}>
        <Text style={styles.noPesanan}>Pesanan No. {noPesanan}</Text>
        <Text style={styles.statusPesanan(statusPesanan)}>
          {labelStatus(statusPesanan)}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default PesananAktif;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#4DA6FF',
    borderRadius: 5,
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 18,
    flexDirection: 'row',
  },
  label: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginLeft: 12,
  },
  noPesanan: {
    fontSize: 18,
    fontFamily: 'HindMadurai-SemiBold',
    color: 'white',
  },
  statusPesanan: type => ({
    fontSize: 18,
    fontFamily: 'HindMadurai-Regular',
    color: type === 1 ? '#D5E4C3' : '#C47482',
  }),
});
