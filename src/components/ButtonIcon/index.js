import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  EkspressIcon,
  KarpetIcon,
  KiloanIcon,
  PointIcon,
  SaldoIcon,
  SatuanIcon,
  StrikaAjaIcon,
  VIPIcon,
} from '../../assets';

const ButtonIcon = ({tittle, type, order}) => {
  const Icon = () => {
    if (tittle === 'Top Up') return <SaldoIcon />;
    if (tittle === 'Get Point') return <PointIcon />;
    if (tittle === 'Kiloan') return <KiloanIcon />;
    if (tittle === 'Satuan') return <SatuanIcon />;
    if (tittle === 'Karpet') return <KarpetIcon />;
    if (tittle === 'VIP') return <VIPIcon />;
    if (tittle === 'Strika Aja') return <StrikaAjaIcon />;
    if (tittle === 'Ekspress') return <EkspressIcon />;
    return <SaldoIcon />;
  };

  return (
    <TouchableOpacity
      style={type === 'square' ? styles.squareBox : styles.rectangleBox(order)}>
      <View>
        <Icon />
      </View>
      <Text style={styles.iconText}>{tittle}</Text>
    </TouchableOpacity>
  );
};

export default ButtonIcon;

const styles = StyleSheet.create({
  squareBox: {
    backgroundColor: '#0783FF',
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
    aspectRatio: 1,
  },
  rectangleBox: type => ({
    backgroundColor: '#0783FF',
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    height: 148,
    width: 100,
    marginRight: type === 'last' ? 0 : 15,
  }),
  iconText: {
    color: 'white',
    fontFamily: 'HindMadurai-Bold',
    fontSize: 12,
  },
});
