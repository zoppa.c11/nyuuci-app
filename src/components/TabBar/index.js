import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {LocationIcon} from '../../assets';
import Tab from '../Tab';

function TabBar({state, descriptors, navigation}) {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <View style={styles.location}>
          <LocationIcon />
          <View>
            <Text
              style={{
                color: '#BFBFBF',
                fontFamily: 'HindMadurai-Medium',
                fontSize: 18,
              }}>
              Location
            </Text>
            <Text
              style={{
                color: '#4D4D4D',
                fontFamily: 'HindMadurai-Medium',
                fontSize: 18,
              }}>
              Jl. Pasir Kaliki No.123, Kota Bandung, Jawa Barat
            </Text>
          </View>
        </View>
        <View style={styles.containerTab}>
          {state.routes.map((route, index) => {
            const {options} = descriptors[route.key];
            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            return (
              <Tab
                key={index}
                label={label}
                onPress={onPress}
                onLongPress={onLongPress}
                isFocused={isFocused}
              />
            );
          })}
        </View>
      </View>
    </View>
  );
}

export default TabBar;

const screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    bottom: 42,
    width: screenWidth,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  container: {
    flexDirection: 'column',
    backgroundColor: '#F2F2F2',
    width: '100%',
    borderRadius: 20,
    padding: 16,
    justifyContent: 'space-between',
  },
  containerTab: {
    flexDirection: 'row',
    backgroundColor: 'white',
    // height: screenHeight * 0.07,
    justifyContent: 'space-around',
    borderRadius: 20,
    paddingVertical: 14,
  },
  location: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: 13,
  },
});
