import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {
  AkunActiveIcon,
  AkunInactiveIcon,
  HomeActiveIcon,
  HomeInactiveIcon,
  PesananActiveIcon,
  PesananInactiveIcon,
} from '../../assets';

const Tab = ({onPress, isFocused, onLongPress, label}) => {
  const Icon = () => {
    if (label === 'Home')
      return isFocused ? <HomeActiveIcon /> : <HomeInactiveIcon />;
    if (label === 'Pesanan')
      return isFocused ? <PesananActiveIcon /> : <PesananInactiveIcon />;
    if (label === 'Akun')
      return isFocused ? <AkunActiveIcon /> : <AkunInactiveIcon />;

    return <HomeActiveIcon />;
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <Icon />
    </TouchableOpacity>
  );
};
export default Tab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
