import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ButtonIcon from '../ButtonIcon';

const Saldo = () => {
  return (
    <View style={styles.box}>
      <View style={styles.nominal}>
        <View style={styles.boxPointSaldo}>
          <Text style={styles.nominalText}>Rp. 125.000</Text>
        </View>
        <View style={styles.boxPointSaldo}>
          <Text style={styles.nominalText}>245 Point</Text>
        </View>
      </View>
      <ButtonIcon tittle="Top Up" type="square" />
      <ButtonIcon tittle="Get Point" type="square" />
    </View>
  );
};

export default Saldo;

const styles = StyleSheet.create({
  box: {
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 75,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 25,
    paddingVertical: 32,
  },
  boxPointSaldo: {
    color: 'white',
    backgroundColor: '#0783FF',
    borderRadius: 10,
    paddingVertical: 3,
    paddingRight: 9,
    paddingLeft: 20,
  },
  nominal: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  nominalText: {
    fontFamily: 'HindMadurai-SemiBold',
    fontSize: 18,
    color: 'white',
    textAlign: 'right',
  },
});
