import React from 'react';
import {StyleSheet, Text, View, Dimensions, ScrollView} from 'react-native';
import {ButtonIcon, PesananAktif, Saldo} from '../../components';

const Pesanan = [
  {
    id: 1,
    noPesanan: '0001455',
    statusPesanan: 1,
  },
  {
    id: 2,
    noPesanan: '0001241',
    statusPesanan: 2,
  },
  {
    id: 3,
    noPesanan: '0006346',
    statusPesanan: 2,
  },
  {
    id: 4,
    noPesanan: '0002743',
    statusPesanan: 1,
  },
];

const Home = () => {
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.backgroundBox}>
          <View style={styles.hello}>
            <Text style={styles.greeting}>Selamat Datang ,</Text>
            <Text style={styles.username}>Andi !</Text>
            <Text style={styles.reminder}>
              Jangan lupa isi saldo dan pointmu ! Untuk mempermudah dalam
              memilih layanan yang tersedia .
            </Text>
          </View>
          <Saldo />
        </View>
        <View>
          <Text style={styles.layanan}>Layanan Kami</Text>
          <View style={styles.layananCard}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <ButtonIcon tittle="Kiloan" type="rectangle" />
              <ButtonIcon tittle="Satuan" type="rectangle" />
              <ButtonIcon tittle="VIP" type="rectangle" />
              <ButtonIcon tittle="Karpet" type="rectangle" />
              <ButtonIcon tittle="Strika Aja" type="rectangle" />
              <ButtonIcon tittle="Ekspress" type="rectangle" order="last"/>
            </ScrollView>
          </View>
        </View>
        <View>
          <Text style={styles.layanan}>Pesanan Aktif</Text>
          <View style={styles.pesananAktif}>
            {Pesanan.map(item => (
              <PesananAktif
                key={item.id}
                noPesanan={item.noPesanan}
                statusPesanan={item.statusPesanan}
              />
            ))}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {backgroundColor: 'white', flex: 1},
  backgroundBox: {
    backgroundColor: '#4DA6FF',
    marginTop: 8,
    marginLeft: 8,
    marginRight: 8,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 75,
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 17,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  hello: {
    marginTop: screenHeight * 0.07,
    marginBottom: 18,
    flexDirection: 'column',
  },
  greeting: {
    fontSize: 36,
    color: 'white',
    fontFamily: 'HindMadurai-SemiBold',
  },
  username: {
    fontSize: 36,
    color: 'white',
    fontFamily: 'HindMadurai-SemiBold',
  },
  reminder: {
    fontSize: 12,
    color: 'white',
    fontFamily: 'HindMadurai-Medium',
  },
  layanan: {
    fontSize: 24,
    fontFamily: 'HindMadurai-SemiBold',
    paddingVertical: 26,
    paddingHorizontal: 30,
  },
  layananCard: {
    flexDirection: 'row',
    paddingLeft: 30,
    paddingRight: 30,
  },
  contentContainer: {
    paddingBottom: 240,
  },
  pesananAktif: {
    paddingHorizontal: 30,
    flexDirection: 'column',
    flex: 1,
  },
});
