import HomeActiveIcon from './HomeActive.svg';
import PesananActiveIcon from './PesananActive.svg';
import AkunActiveIcon from './AkunActive.svg';
import HomeInactiveIcon from './HomeInactive.svg';
import PesananInactiveIcon from './PesananInactive.svg';
import AkunInactiveIcon from './AkunInactive.svg';
import LocationIcon from './LocationIcon.svg';
import SaldoIcon from './Saldo.svg';
import PointIcon from './Point.svg';
import KiloanIcon from './KiloanIcon.svg';
import SatuanIcon from './SatuanIcon.svg';
import KarpetIcon from './KarpetIcon.svg';
import VIPIcon from './VIPIcon.svg';
import StrikaAjaIcon from './StrikaAjaIcon.svg';
import EkspressIcon from './EkspressIcon.svg';
import PesananAktifIcon from './PesananAktifIcon.svg';

export {
  HomeActiveIcon,
  HomeInactiveIcon,
  PesananActiveIcon,
  PesananInactiveIcon,
  AkunActiveIcon,
  AkunInactiveIcon,
  LocationIcon,
  SaldoIcon,
  PointIcon,
  KiloanIcon,
  SatuanIcon,
  KarpetIcon,
  VIPIcon,
  StrikaAjaIcon,
  EkspressIcon,
  PesananAktifIcon,
};
